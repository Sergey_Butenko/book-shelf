<?php

Route::post('books', 'BookController@create');
Route::get('books', 'BookController@paginate');
Route::get('books/{id}', 'BookController@get');
Route::post('books/{id}', 'BookController@update');
Route::delete('books/{id}', 'BookController@delete');

Route::get('authors', 'AuthorController@all');
Route::get('genres', 'GenreController@all');
Route::get('languages', 'LanguageController@all');
