<?php

namespace App\Providers;

use App\Contracts\AuthorServiceContract;
use App\Contracts\BookServiceContract;
use App\Contracts\GenreServiceContract;
use App\Contracts\LanguageServiceContract;
use App\Contracts\MediaServiceContract;
use App\Repositories\AuthorRepository;
use App\Repositories\AuthorRepositoryEloquent;
use App\Repositories\BookRepository;
use App\Repositories\BookRepositoryEloquent;
use App\Repositories\GenreRepository;
use App\Repositories\GenreRepositoryEloquent;
use App\Repositories\LanguageRepository;
use App\Repositories\LanguageRepositoryEloquent;
use App\Repositories\MediaRepository;
use App\Repositories\MediaRepositoryEloquent;
use App\Services\AuthorService;
use App\Services\BookService;
use App\Services\GenreService;
use App\Services\LanguageService;
use App\Services\MediaService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(MediaServiceContract::class, MediaService::class);
        app()->bind(MediaRepository::class, MediaRepositoryEloquent::class);
        app()->bind(BookServiceContract::class, BookService::class);
        app()->bind(BookRepository::class, BookRepositoryEloquent::class);
        app()->bind(AuthorRepository::class, AuthorRepositoryEloquent::class);
        app()->bind(GenreRepository::class, GenreRepositoryEloquent::class);
        app()->bind(LanguageRepository::class, LanguageRepositoryEloquent::class);
        app()->bind(AuthorServiceContract::class, AuthorService::class);
        app()->bind(GenreServiceContract::class, GenreService::class);
        app()->bind(LanguageServiceContract::class, LanguageService::class);
    }
}
