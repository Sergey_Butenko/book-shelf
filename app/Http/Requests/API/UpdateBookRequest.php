<?php

namespace App\Http\Requests\API;

use App\Rules\Isbn13Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBookRequest extends FormRequest
{
    /**
     * @param null $keys
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all();
        $data['id'] = $this->route('id');

        return $data;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'           => 'required|integer|exists:books,id',
            'title'        => 'required|string|max:255',
            'author_id'    => 'required|integer|exists:authors,id',
            'genre_id'     => 'required|integer|exists:genres,id',
            'language_id'  => 'required|integer|exists:languages,id',
            'published_at' => 'required|date',
            'isbn_number'  => [
                'required',
                'numeric',
                new Isbn13Rule()
            ],
            'image'        => 'image|nullable'
        ];
    }
}
