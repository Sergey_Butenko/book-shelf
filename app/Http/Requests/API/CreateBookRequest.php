<?php

namespace App\Http\Requests\API;

use App\Rules\Isbn13Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|string|max:255',
            'author_id'    => 'required|integer|exists:authors,id',
            'genre_id'     => 'required|integer|exists:genres,id',
            'language_id'  => 'required|integer|exists:languages,id',
            'published_at' => 'required|date',
            'isbn_number'  => [
                'required',
                'numeric',
                new Isbn13Rule()
            ],
            'image'        => 'image|nullable'
        ];
    }
}
