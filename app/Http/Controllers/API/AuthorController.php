<?php

namespace App\Http\Controllers\API;

use App\Contracts\AuthorServiceContract;
use App\Transformers\AuthorTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    /**
     * @var AuthorServiceContract
     */
    private $authorService;

    /**
     * AuthorController constructor.
     * @param AuthorServiceContract $authorService
     */
    public function __construct(AuthorServiceContract $authorService)
    {
        $this->authorService = $authorService;
    }

    /**
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {
        $authors = $this->authorService->all();

        return fractal($authors, new AuthorTransformer())->respond();
    }
}
