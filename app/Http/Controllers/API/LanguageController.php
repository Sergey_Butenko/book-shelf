<?php

namespace App\Http\Controllers\API;

use App\Contracts\LanguageServiceContract;
use App\Transformers\LanguageTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    /**
     * @var LanguageServiceContract
     */
    private $languageService;

    /**
     * LanguageController constructor.
     * @param LanguageServiceContract $languageService
     */
    public function __construct(LanguageServiceContract $languageService)
    {
        $this->languageService = $languageService;
    }

    /**
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {
        $languages = $this->languageService->all();

        return fractal($languages, new LanguageTransformer())->respond();
    }
}
