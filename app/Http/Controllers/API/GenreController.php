<?php

namespace App\Http\Controllers\API;

use App\Contracts\GenreServiceContract;
use App\Transformers\GenreTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GenreController extends Controller
{
    /**
     * @var GenreServiceContract
     */
    private $genreService;

    /**
     * GenreController constructor.
     * @param GenreServiceContract $genreService
     */
    public function __construct(GenreServiceContract $genreService)
    {
        $this->genreService = $genreService;
    }

    /**
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {
        $genres = $this->genreService->all();

        return fractal($genres, new GenreTransformer())->respond();
    }
}
