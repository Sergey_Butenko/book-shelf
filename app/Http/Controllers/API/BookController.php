<?php

namespace App\Http\Controllers\API;

use App\Contracts\BookServiceContract;
use App\Http\Requests\API\CreateBookRequest;
use App\Http\Requests\API\DeleteBookRequest;
use App\Http\Requests\API\GetBookRequest;
use App\Http\Requests\API\UpdateBookRequest;
use App\Transformers\BookTransformer;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class BookController extends Controller
{
    /**
     * @var BookServiceContract
     */
    private $bookService;

    /**
     * BookController constructor.
     * @param BookServiceContract $bookService
     */
    public function __construct(BookServiceContract $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * @return JsonResponse
     */
    public function paginate(): JsonResponse
    {
        $books = $this->bookService->paginate();

        return fractal($books, new BookTransformer())->respond();
    }

    /**
     * @param CreateBookRequest $request
     * @return JsonResponse
     */
    public function create(CreateBookRequest $request): JsonResponse
    {
        $book = $this->bookService->create(
            $request->title,
            $request->author_id,
            $request->genre_id,
            $request->language_id,
            $request->published_at,
            $request->isbn_number,
            $request->image
        );

        return fractal($book, new BookTransformer())->respond(Response::HTTP_CREATED);
    }

    /**
     * @param GetBookRequest $request
     * @return JsonResponse
     */
    public function get(GetBookRequest $request): JsonResponse
    {
        $book = $this->bookService->get($request->id);

        return fractal($book, new BookTransformer())->respond(Response::HTTP_OK);
    }

    /**
     * @param UpdateBookRequest $request
     * @return JsonResponse
     */
    public function update(UpdateBookRequest $request): JsonResponse
    {
        $book = $this->bookService->update(
            $request->id,
            $request->title,
            $request->author_id,
            $request->genre_id,
            $request->language_id,
            $request->published_at,
            $request->isbn_number,
            $request->image
        );

        return fractal($book, new BookTransformer())->respond(Response::HTTP_OK);
    }

    /**
     * @param DeleteBookRequest $request
     * @return JsonResponse
     */
    public function delete(DeleteBookRequest $request): JsonResponse
    {
        $this->bookService->delete($request->id);

        return response()->json(['Deleted.']);
    }
}
