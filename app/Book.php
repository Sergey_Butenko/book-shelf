<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 * @package App
 *
 * @property string $title
 * @property integer $author_id
 * @property integer $genre_id
 * @property integer $language_id
 * @property Carbon $published_at
 * @property string $isbn_number
 * @property integer $media_id
 * @property Author $author
 * @property Genre $genre
 * @property Language $language
 */
class Book extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'author_id',
        'genre_id',
        'language_id',
        'published_at',
        'isbn_number',
        'media_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'published_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }
}
