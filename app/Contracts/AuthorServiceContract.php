<?php


namespace App\Contracts;


use Illuminate\Database\Eloquent\Collection;

interface AuthorServiceContract
{
    /**
     * @return Collection
     */
    public function all(): Collection;
}