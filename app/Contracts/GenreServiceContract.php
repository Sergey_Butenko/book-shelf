<?php


namespace App\Contracts;


use Illuminate\Database\Eloquent\Collection;

interface GenreServiceContract
{
    /**
     * @return Collection
     */
    public function all(): Collection;
}