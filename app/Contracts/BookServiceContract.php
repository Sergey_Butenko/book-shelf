<?php


namespace App\Contracts;


use App\Book;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;

interface BookServiceContract
{
    /**
     * @return mixed
     */
    public function paginate(): LengthAwarePaginator;

    /**
     * @param int $id
     * @return mixed
     */
    public function get(int $id): Book;

    /**
     * @param string $title
     * @param int $authorId
     * @param int $genreId
     * @param int $languageId
     * @param $publishedAt
     * @param string $isbnNumber
     * @param UploadedFile|null $image
     * @return mixed
     */
    public function create(
        string $title,
        int $authorId,
        int $genreId,
        int $languageId,
        $publishedAt,
        string $isbnNumber,
        UploadedFile $image = null
    ): Book;

    /**
     * @param int $bookId
     * @param string $title
     * @param int $authorId
     * @param int $genreId
     * @param int $languageId
     * @param $publishedAt
     * @param string $isbnNumber
     * @param UploadedFile|null $image
     * @return Book
     */
    public function update(
        int $bookId,
        string $title,
        int $authorId,
        int $genreId,
        int $languageId,
        $publishedAt,
        string $isbnNumber,
        UploadedFile $image = null
    ): Book;

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id): int;
}