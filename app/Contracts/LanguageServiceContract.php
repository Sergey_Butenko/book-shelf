<?php


namespace App\Contracts;


use Illuminate\Database\Eloquent\Collection;

interface LanguageServiceContract
{
    /**
     * @return Collection
     */
    public function all(): Collection;
}