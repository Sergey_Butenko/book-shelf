<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Isbn\Isbn;

class Isbn13Rule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $isbn = new Isbn();

        return $isbn->validation->isbn13($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Not a valid isbn-13 number.';
    }
}
