<?php


namespace App\Services;


use App\Book;
use App\Contracts\BookServiceContract;
use App\Contracts\MediaServiceContract;
use App\Repositories\BookRepository;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;

class BookService implements BookServiceContract
{
    /**
     * @var BookRepository
     */
    private $bookRepository;

    /**
     * @var MediaServiceContract
     */
    private $mediaService;

    /**
     * BookService constructor.
     * @param BookRepository $bookRepository
     * @param MediaServiceContract $mediaService
     */
    public function __construct(BookRepository $bookRepository, MediaServiceContract $mediaService)
    {
        $this->bookRepository = $bookRepository;
        $this->mediaService = $mediaService;
    }

    /**
     * @return mixed
     */
    public function paginate(): LengthAwarePaginator
    {
        return $this->bookRepository->paginate();
    }

    /**
     * @param int $id
     * @return Book
     */
    public function get(int $id): Book
    {
        try {
            $book = $this->bookRepository->find($id);
        } catch (\Exception $exception) {
            abort(Response::HTTP_NOT_FOUND, 'Book not found.');
        }

        return $book;
    }

    /**
     * @param string $title
     * @param int $authorId
     * @param int $genreId
     * @param int $languageId
     * @param $publishedAt
     * @param string $isbnNumber
     * @param UploadedFile|null $image
     * @return Book
     */
    public function create(
        string $title,
        int $authorId,
        int $genreId,
        int $languageId,
        $publishedAt,
        string $isbnNumber,
        UploadedFile $image = null
    ): Book
    {
        try {
            $input = [
                'title'        => $title,
                'author_id'    => $authorId,
                'genre_id'     => $genreId,
                'language_id'  => $languageId,
                'published_at' => $publishedAt,
                'isbn_number'  => $isbnNumber
            ];
            $input = $this->handleImage($input, $image);

            $book = $this->bookRepository->create($input);
        } catch (\Exception $exception) {
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to create new book.');
        }

        return $book;
    }

    /**
     * @param int $bookId
     * @param string $title
     * @param int $authorId
     * @param int $genreId
     * @param int $languageId
     * @param $publishedAt
     * @param string $isbnNumber
     * @param UploadedFile|null $image
     * @return Book
     */
    public function update(
        int $bookId,
        string $title,
        int $authorId,
        int $genreId,
        int $languageId,
        $publishedAt,
        string $isbnNumber,
        UploadedFile $image = null
    ): Book
    {
        try {
            $input = [
                'title'        => $title,
                'author_id'    => $authorId,
                'genre_id'     => $genreId,
                'language_id'  => $languageId,
                'published_at' => $publishedAt,
                'isbn_number'  => $isbnNumber
            ];
            $input = $this->handleImage($input, $image);

            $book = $this->bookRepository->update($input, $bookId);
        } catch (\Exception $exception) {
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to update a book.');
        }

        return $book;
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete(int $id): int
    {
        try {
            $book = $this->bookRepository->delete($id);
        } catch (\Exception $exception) {
            abort(Response::HTTP_EXPECTATION_FAILED, 'Failed to delete a book.');
        }

        return $book;
    }

    /**
     * @param array $input
     * @param UploadedFile $image
     * @return array
     */
    private function handleImage(array $input, UploadedFile $image = null)
    {
        if ($image) {
            $media = $this->mediaService->uploadFile($image);
            $input['media_id'] = $media->id;
        }

        return $input;
    }
}