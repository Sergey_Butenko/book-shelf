<?php


namespace App\Services;


use App\Contracts\AuthorServiceContract;
use App\Repositories\AuthorRepository;
use Illuminate\Database\Eloquent\Collection;

class AuthorService implements AuthorServiceContract
{
    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    /**
     * AuthorService constructor.
     * @param AuthorRepository $authorRepository
     */
    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->authorRepository->all();
    }
}