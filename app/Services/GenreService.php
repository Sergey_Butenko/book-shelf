<?php


namespace App\Services;


use App\Contracts\GenreServiceContract;
use App\Repositories\GenreRepository;
use Illuminate\Database\Eloquent\Collection;

class GenreService implements GenreServiceContract
{
    /**
     * @var GenreRepository
     */
    private $genreRepository;

    /**
     * GenreService constructor.
     * @param GenreRepository $genreRepository
     */
    public function __construct(GenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->genreRepository->all();
    }
}