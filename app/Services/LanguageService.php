<?php


namespace App\Services;


use App\Contracts\LanguageServiceContract;
use App\Repositories\LanguageRepository;
use Illuminate\Database\Eloquent\Collection;

class LanguageService implements LanguageServiceContract
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * LanguageService constructor.
     * @param LanguageRepository $languageRepository
     */
    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->languageRepository->all();
    }
}