<?php

namespace App\Transformers;

use App\Language;
use League\Fractal\TransformerAbstract;

class LanguageTransformer extends TransformerAbstract
{
    /**
     * @param Language $language
     * @return array
     */
    public function transform(Language $language)
    {
        return [
            'id' => $language->id,
            'code' => $language->code
        ];
    }
}
