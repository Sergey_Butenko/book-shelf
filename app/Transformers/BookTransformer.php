<?php

namespace App\Transformers;

use App\Book;
use League\Fractal\TransformerAbstract;

class BookTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = ['author', 'genre', 'language', 'media'];

    /**
     * @param Book $book
     * @return array
     */
    public function transform(Book $book)
    {
        return [
            'id' => $book->id,
            'title' => $book->title,
            'isbn_number' => $book->isbn_number,
            'published_at' => $book->published_at->format('Y-m-d')
        ];
    }

    /**
     * @param Book $book
     * @return \League\Fractal\Resource\Item
     */
    public function includeAuthor(Book $book)
    {
        return $this->item($book->author, new AuthorTransformer());
    }

    /**
     * @param Book $book
     * @return \League\Fractal\Resource\Item
     */
    public function includeGenre(Book $book)
    {
        return $this->item($book->genre, new GenreTransformer());
    }

    /**
     * @param Book $book
     * @return \League\Fractal\Resource\Item
     */
    public function includeLanguage(Book $book)
    {
        return $this->item($book->language, new LanguageTransformer());
    }

    /**
     * @param Book $book
     * @return \League\Fractal\Resource\Item
     */
    public function includeMedia(Book $book)
    {
        if(!$book->image) {
            return null;
        }

        return $this->item($book->image, new MediaTransformer());
    }
}
