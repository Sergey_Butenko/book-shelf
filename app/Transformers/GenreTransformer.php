<?php

namespace App\Transformers;

use App\Genre;
use League\Fractal\TransformerAbstract;

class GenreTransformer extends TransformerAbstract
{
    /**
     * @param Genre $genre
     * @return array
     */
    public function transform(Genre $genre)
    {
        return [
            'id' => $genre->id,
            'name' => $genre->name
        ];
    }
}
