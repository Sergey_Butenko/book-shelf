import BooksComponent from './components/BooksComponent.vue';
import BookEditComponent from './components/BookEditComponent.vue';
import BookViewComponent from './components/BookViewComponent.vue';
import BookCreateComponent from './components/BookCreateComponent.vue';

export const routes = [
    {
        path: '',
        component: BooksComponent
    },
    {
        path: '/books',
        component: BooksComponent
    },
    {
        path: '/books/create',
        component: BookCreateComponent,
        name: 'book-create'
    },
    {
        path: '/books/:id/edit',
        component: BookEditComponent,
        name: 'book-edit'
    },
    {
        path: '/books/:id',
        component: BookViewComponent,
        name: 'book-view'
    }
];
