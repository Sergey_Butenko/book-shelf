
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import VueRouter from 'vue-router';
import App from './components/App.vue';
import { routes } from './routes';
Vue.use(VueRouter);
Vue.component('app-books', require('./components/BooksComponent.vue'));
Vue.component('app-navbar', require('./components/NavBarComponent.vue'));
Vue.component('form-errors', require('./components/FormErrorsComponent.vue'));

const router = new VueRouter({
    routes
});

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
