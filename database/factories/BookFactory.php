<?php

use Faker\Generator as Faker;

$factory->define(\App\Book::class, function (Faker $faker) {
    return [
        'title' => $faker->jobTitle,
        'author_id' => function() {
            return factory(\App\Author::class)->create()->id;
        },
        'genre_id' => function() {
            return factory(\App\Genre::class)->create()->id;
        },
        'language_id' => function() {
            return factory(\App\Language::class)->create()->id;
        },
        'published_at' => $faker->date(),
        'isbn_number' => $faker->isbn13,
        'media_id' => function() {
            return factory(App\Media::class)->create()->id;
        }
    ];
});
