<?php

use Faker\Generator as Faker;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

$factory->define(\App\Media::class, function (Faker $faker) {
    $image = $faker->image();
    $file = new File($image);
    $filename = uniqid() . '.' . $file->extension();
    Storage::putFileAs('images', $file, $filename);
    return [
        'filename' => $filename,
        'mime_type' => $file->getMimeType(),
        'size' => $file->getSize()
    ];
});
